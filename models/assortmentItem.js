var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var itemSchema = new mongoose.Schema({
    //_id: String,
    name: {
        type:String,
        required: true,
        unique: true
    },
    price: Number,
    type: String,
    user: String
    // _created_at: Date,
    // _updated_at: Date,
    // quantity: Number,
    // type: String,
    // dateCreated: String
},
    {
        collection: 'assortmentItems'
    }
);

itemSchema.plugin(uniqueValidator);

var AssortmentItem = mongoose.model('AssortmentItem', itemSchema);

module.exports = AssortmentItem;