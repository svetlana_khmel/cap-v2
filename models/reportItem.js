var mongoose = require('mongoose');

var itemSchema = new mongoose.Schema({
        //_id: String,
        name: String,
        price: Number,
        type: String,
        user: String,
        created_at: String
        // _updated_at: Date,
        // quantity: Number,
        // type: String,
        // dateCreated: String
    },
    {
        collection: 'reportItems'
    });

var ReportItem = mongoose.model('ReportItem', itemSchema);

module.exports = ReportItem;