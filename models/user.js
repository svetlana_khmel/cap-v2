var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
//var Schema = mongoose.Schema;

var schema = new mongoose.Schema(
    {
        username: {
            type: String,
            unique: true,
            required: true
        },
        password: {
            type: String,
            required: true
        }
        //,id: String
    },
        {
            collection: 'users'
        }
    );


schema.plugin(uniqueValidator);

var User = mongoose.model('User', schema);
module.exports = User;

//exports.User = mongoose.model('User', schema);