var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');

var orderSchema = new mongoose.Schema({
        timeForOneItem: String, //In milliseconds
        status: String, // ready
        key: String,
        models: [],
        user: String,
        created_at: String
    },
    {
        collection: 'orderItems'
    });


orderSchema.plugin(uniqueValidator);
var OrderItem = mongoose.model('OrderItem', orderSchema);

module.exports = OrderItem;
