'use strict';

module.exports = function (grunt) {
    // load all grunt tasks
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-copy');


    grunt.initConfig({
        watch: {
            // if any .less file changes in directory "public/css/" run the "less"-task.
            files: "public/styles/less/*.less",
            tasks: ["less", "concat_css"]
        },

        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true, src: [
                        '../mode_modules/backbone/backbone.js',
                        //'../mode_modules/handlebars/lib/handlebars.js',
                       // '../node_modules/underscore/underscore.js'

                    ], dest: 'public/js/vendor/', filter: 'isFile'}
                ]
            }
        },

        // "less"-task configuration
        less: {
            // production config is also available
            development: {
                options: {
                    // Specifies directories to scan for @import directives when parsing.
                    // Default value is the directory of the source, which is probably what you want.
                    paths: ["public/stylesheets/"]
                },
                files: {
                    // compilation.css  :  source.less
                    "public/styles/css/app.css": "public/styles/less/app.less"
                }
            }
        },
        concat_css: {
            options: {
                // Task-specific options go here.
            },
            all: {
                src: ["public/styles/**/*.css"],
                dest: "public/styles/css/app.css"
            }
        }
    });
    // the default task (running "grunt" in console) is "watch"
    grunt.registerTask('default', ['watch']);
    //require("load-grunt-tasks")(grunt);
    grunt.registerTask('copy', ['copy']);


};