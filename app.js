var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var index = require('./routes/index');
var assortment = require('./routes/assortment');
var report = require('./routes/report');
var users = require('./routes/users');
var orders = require('./routes/orders');
var order = require('./routes/order');

//  Authentication

var passport = require('passport');
//var LocalStrategy = require('passport-local').Strategy;
// var statistic = require('./routes/statistic');

// var users = require('./routes/users');
var mongoose = require('mongoose');

//mongodb://localhost/capdb

mongoose.connect('mongodb://test2:test2@ds033036.mlab.com:33036/capapp', function(err) {
    if(err) {
        console.log('connection error', err);
    } else {
        console.log('connection successful');
    }
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.bodyParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(session({
//     secret: 'keyboard cat',
//     resave: true,
//     saveUninitialized: true,
//     cookie: { secure: true }
// }));

app.use(session({
    secret: 'secrettexthere',
    saveUninitialized: true,
    resave: true
    // using store session on MongoDB using express-session + connect
    // store: new MongoStore({
    //     url: config.urlMongo,
    //     collection: 'sessions'
    // })
}));

app.use(passport.initialize());
app.use(session());

app.use(function (req, res, next) {
    res.locals.user = req.user || null;

    // if(req.originalUrl === '/login') {
    //     next();
    // }

    // var rex = /\/login/;
    // if(!req.session.user && rex.test(req.originalUrl)){
    //    // res.redirect('/#logout');
    //     next();
    // }


    // var rex = /\/api\/users/;
    // var rex1 = /\/api\/users\/login/;
    // var rex2 = /\/login/;
    //
    // if(!req.session.user && rex1.test(req.originalUrl) || !req.session.user && rex2.test(req.originalUrl)) {
    //     next();
    // } else if (!req.session.user) {
    //     res.redirect('/#logout');
    // }


    // if(!req.session.user && rex.test(req.originalUrl) && !rex1.test(req.originalUrl)) {
    //     res.redirect('/#logout');
    // }

    // var rex = /\/api\/users\/login/;
    //
    // if(!req.session.user && rex.test(req.originalUrl)){
    //     next();
    // }

    //
    // var rex = /\/api\/users/;
    // var rex1 = /login/;
    //
    //
    // if(!req.session.user && rex.test(req.originalUrl) && !rex1.test(req.originalUrl) ){
    //     res.redirect('/#logout');
    // }

    var rex = /\/assortment/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

    var rex = /\/api\/assortment/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

    var rex = /\/report/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

    var rex = /\/api\/report/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

    var rex = /\/statistic/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

    var rex = /\/api\/statistic/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

    var rex = /\/todo/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

     var rex = /\/api\/todo/;

    if(!req.session.user && rex.test(req.originalUrl)){
        res.redirect('/#logout');
    }

   next();
});

app.use('/api/assortment', assortment);
app.use('/api/report', report);
app.use('/api/users', users);
app.use('/api/todo', orders);

app.use('/order', order);

app.get('/assortment', function (req, res) {
    res.redirect('/#assortment');
});
app.get('/report', function (req, res) {
    res.redirect('/#report');
});
app.get('/statistic', function (req, res) {
    res.redirect('/#report');
});
app.get('/login', function (req, res) {
    res.redirect('/#login');
});
app.get('/logout', function (req, res) {
    res.redirect('/#logout');
});
app.get('/todo', function (req, res) {
    res.redirect('/#todo');
});

// app.get('/')function (req, res) {
    // res.send(index);
// });
// app.get(/^\/(\/\w+)*$/, function(req, res) {
//     console.log('back');
//     res.sendFile('./public/index.html');
// });
app.use('/', index);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
