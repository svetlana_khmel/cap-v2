# README #

* Backbone, - Requirejs, - Handlebars, - Node, - Express, - Socket.io - Mongodb


OVERVIEW:

This application helps calculate small sales (as coffee etc).
This is not for government reports, it is for private entrepreneurs who wants to calculate sales and see sale’s statistic in a easiest way.

Demo is available on https://vast-meadow-96220.herokuapp.com/


![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1489498680/1_p3p8t8.png)


![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1489498639/4_mq1jln.png)

![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1489498639/3_sjhk2h.png)

![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1489498639/7_kq51ra.png)

![Scheme](http://res.cloudinary.com/dmt6v2tzo/image/upload/v1489498639/8_hjjdbt.png)


HOW TO USE:

Assortment page: Create an assortment list.

Report page:  Add item to ’Cheque’. When item is sold, submit it.

You can see your how many items has been sold in day.

Statistic page: You can see on chats how many items has been sold in a day / month /year (Quantity and sum)

TO DO LIST:

1. Access by url /todo/

When check is submitted, you can find the order in ‘To do list’ (/todo);
When order is ready mark it as ready. Your customer will be able to see it on ready list (Or marked as ready)

When your customer has an order, mark it as ‘done’.

ORDER STATUS:

Customer can track his order's status on /order/[** ORDER'S NUMBER **]




### What is this repository for? ###

Goal:
* to create useful applications for sales using:
- Backbone,
- Requirejs,
- Handlebars,
- Node,
- Express,
- Socket.io
- Mongodb

### How do I get set up? ###

Install NVM:

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

* Use for install dependencies
npm install
* Run:
DEBUG=cap-v2:* npm start

* Summary of set up

* Run grunt watcher:
(By default compiles less to css, merge css);
grunt watch

* Configuration
* Dependencies
* Database configuration

* Run db server:
mongod

* Run db client:
mongo


* How to run tests
* Deployment instructions


### Helpful articles: ###
Backbone:
https://addyosmani.com/backbone-fundamentals/#exercise-2-book-library---your-first-restful-backbone.js-app

Handlebars:
http://tutorialzine.com/2015/01/learn-handlebars-in-10-minutes/


Backbone Collections:
http://rahulrajatsingh.com/2014/07/backbone-tutorial-part-5-understanding-backbone-js-collections/

Mongodb:
https://www.tutorialspoint.com/mongodb/

### Services in use:
https://mlab.com/
https://www.heroku.com/
https://cloudinary.com/

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact