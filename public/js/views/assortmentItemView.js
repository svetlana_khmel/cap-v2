define(['jquery', 'underscore', 'backbone',
    //'handlebars',
    'vendor/handlebars-v4.0.5',
    'models/assortmentItemModel',
    'collections/assortmentCollection',
    'text!../../templates/assortment-item.html',
    'mixins/events'
], function ($, _, Backbone, Handlebars, AssortmentItemModel, AssortmentCollection, template, Events) {

    var AssortmentItemView = Backbone.View.extend({
        model: new AssortmentItemModel(),
        collection: new AssortmentCollection(),
        tagName: 'div',
        className: 'js-assortment-item',
        events: {
            'click .delete':'deleteItem'
        },
        initialize: function (options) {
            this.options = options || {};
        },
        deleteItem: function (e) {
            var self = this;
            console.log('e', e, ' this.model  ', this.model, '  this.collection ', this.collection);
            e.preventDefault();

            var dfd = $.Deferred();
            this.model.set({dfd: dfd});

            dfd
                .done(this.model.destroy(), function (res) {
                    console.log(res);
                    Events.trigger('assortment_item_deleted');
                });

            // $.when( this.model.destroy()) .then(function() {
            //     Events.trigger('assortment_item_deleted');
            // });

            //this.model.destroy();

            //this.model.collection.remove(this.model);
            this.remove();
            this.collection.remove(this.model);
        },
        render: function () {
            var data = Object.assign({}, this.model.attributes, {key: this.cid});
            var templateCompiled = Handlebars.compile(template);
            var html = templateCompiled(data);
            this.$el.prepend(html);
            return this;
        },
        renderItem: function (item) {
        }
    });

    return AssortmentItemView;
});