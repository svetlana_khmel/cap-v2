define(['jquery', 'underscore', 'backbone',
    //'Handlebars',
    'vendor/handlebars-v4.0.5',
    'models/reportItemModel',
    'collections/checkCollection',
    'text!../../templates/report-item.html',
    'views/checkView',
    'views/checkItemView'
], function ($, _, Backbone, Handlebars, ReportItemModel, CheckCollection, template, CheckView, CheckItemView) {

    var ReportItemView = Backbone.View.extend({
        //el: '.js-report-container',
        tagName: 'div',
        className: 'js-report-item',
        events: {
            'click .add':'addToCheck'
        },
        initialize: function (options) {
            this.options = options || {};
        },
        addToCheck: function (e) {
            //$(this.el).css('border','1px solid red');
            //$(this.el).addClass('recently-added').delay(1000).removeClass('recently-added');


            var self = this;
            console.log('e', e, ' this.model  ', this.model, '  this.collection ', this.collection);
            e.preventDefault();

            var checkCollection = new CheckCollection();
            checkCollection.add(this.model);
            var checkView = new CheckView({model: self.model, collection: checkCollection});
            checkView.render();
        },
        deleteItem: function (e) {
            var self = this;
            console.log('e', e, ' this.model  ', this.model, '  this.collection ', this.collection);
            e.preventDefault();
            this.model.destroy();
            this.remove();
            this.collection.remove(this.model,
                {
                    success: function (options) {
                        console.log("Model has been deleted successfully    ", options);
                    },
                    error: function () {
                        console.log("Model has not been deleted    ", options);
                    }
                });
        },
        render: function () {
            var data = Object.assign({}, this.model.attributes, {key: this.cid}, {sold: this.options.sold});
            var templateCompiled = Handlebars.compile(template);
            var html = templateCompiled(data);
            this.$el.prepend(html);
            return this;
        },
        renderItem: function (item) {
        }
    });

    return ReportItemView;
});