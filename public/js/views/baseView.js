define(['jquery','underscore', 'backbone'
], function ($, _, Backbone) {

    var BaseView = Backbone.View.extend({
        initialize: function(options) {
            _.bindAll(this, 'beforeRender', 'render', 'afterRender');
            var _this = this;
            this.render = _.wrap(_this.render, function(render) {
                _this.beforeRender();
                render();
                _this.afterRender();

                return _this;
            });
        }
    });

    return BaseView;
});