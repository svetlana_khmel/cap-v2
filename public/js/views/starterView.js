define([
    'jquery',
    'underscore',
    'backbone',
    // Using the Require.js text! plugin, we are loaded raw text
    // which will be used as our views primary template
    'text!../../templates/test.html'
], function($, _, Backbone, projectListTemplate){
    var StarterView = Backbone.View.extend({
       // el: 'div',
        el: $('.container'),
        render: function(){
            // Using Underscore we can compile our template with data
            var data = {};
            var compiledTemplate = _.template( projectListTemplate, data );
            // Append our compiled template to this Views "el"
            this.$el.append( compiledTemplate );
           // this.$el.html('Hola!!');
            console.log(this);
            //$('#container').html('hola');
            return this;
        }
    });
    // Our module now returns our view
    return StarterView;
});
