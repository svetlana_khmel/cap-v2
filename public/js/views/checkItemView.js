define(['jquery', 'underscore', 'backbone',
    //'Handlebars',
    'vendor/handlebars-v4.0.5',
    'models/assortmentItemModel',
    'text!../../templates/check-view.html'
], function ($, _, Backbone, Handlebars, AssortmentItemModel, template) {

    var CheckItemView = Backbone.View.extend({
       // el: '.js-check-container',
        tagName: 'div',
        className: 'js-check-item',
        events: {
            'click .delete':'deleteItem'
        },
        initialize: function (options) {
            this.options = options || {};
        },
        deleteItem: function (e) {
            var self = this;
            console.log('e', e, ' this.model  ', this.model, '  this.collection ', this.collection);
            e.preventDefault();
            this.model.destroy();
            this.remove();

            var data = _.filter(JSON.parse(sessionStorage.getItem('storage')), function(item){
                if(item._id !== self.model.attributes._id ) {
                    return item;
                }
            });

            sessionStorage.setItem('storage', JSON.stringify(data));

            // this.collection.remove(this.model,
            //     {
            //         success: function (options) {
            //             console.log("Model has been deleted successfully    ", options);
            //         },
            //         error: function () {
            //             console.log("Model has not been deleted    ", options);
            //         }
            //     });
        },
        render: function () {
            var self = this;
            this.collection.each(function (model) {
                console.log("__ ", model);
                var data = Object.assign({}, model.attributes, {key: self.cid});
                var templateCompiled = Handlebars.compile(template);
                var html = templateCompiled(data);
                self.$el.prepend(html);
            });

            return this;
        },
        renderItem: function (item) {
        }
    });

    return CheckItemView;
});