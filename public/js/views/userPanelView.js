define(['jquery', 'underscore', 'backbone',
    'vendor/handlebars-v4.0.5',
    'text!../../templates/user-panel.html',
    'models/user'
], function ($, _, Backbone, Handlebars, template, User) {

    var UserPanelView = Backbone.View.extend({
        model: new User(),
        el: '.js-user-panel',
        events: {
            'click .logout': 'logout'
        },
        initialize: function (options) {
            this.option = options || {};
        },
        logout: function (e) {
            e.preventDefault();
            localStorage.setItem('currentCupUser', '');

            this.$el.empty();

            Backbone.navigate('login', {trigger: true});
        },
        getCurrentUser: function () {
            return localStorage.getItem('currentCupUser');
        },
        render: function () {
            var data = {user: this.getCurrentUser()};
            var templateCompiled = Handlebars.compile(template);
            var html = templateCompiled(data);
            this.$el.html(html);
            return this;
        }
    });

    return UserPanelView;
});