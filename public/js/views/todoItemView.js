define(['jquery', 'underscore', 'backbone',
    //'Handlebars',
    'vendor/handlebars-v4.0.5',
    'models/todoModel',
    'collections/todoCollection',
    'text!../../templates/todo-item.html',
    'mixins/events'
], function ($, _, Backbone, Handlebars, TodoModel, TodoCollection, template, Events) {

    var TodoItemView = Backbone.View.extend({
           // model: new TodoModel(),
            collection: new TodoCollection(),
            tagName: 'div',
            className: 'js-todo-item',
            events: {
                    'click .processing': 'setReadyState',
                    'click .ready': 'setDoneState',
                    'click timer': 'setTimer'
                },
        // events: {
        //     'click .delete':'deleteItem'
        // },
        initialize: function (options) {
            this.options = options || {};
        },

        fetchCollection: function () {
            this.option = options || {};
            var self = this;
            this.collection = new TodoCollection();

            this.collection.fetch({
                success: function() {
                    //self.renderList(self.collection);
                    console.log('Collection ......', self.collection)
                },
                error: function (collection, response, options) {
                    console.log('Error on fetch: ', collection, 'Error on fetch: ', response, 'Error on fetch: ', options)
                }
            });
        },
        setReadyState: function(e){
                var self = this;
                e.preventDefault();

                $(e.target)
                    .html('Ready')
                    .removeClass('processing')
                    .addClass('ready');

            this.collection.fetch({
                success: function() {
                    var model = self.collection.get(self.id);

                    model.set({'status': 'ready'});
                    model.save();

                    Events.socketEvents('postMessage', {status: 'ready', model: model});

                },
                error: function (collection, response, options) {
                    console.log('Error on fetch: ', collection, 'Error on fetch: ', response, 'Error on fetch: ', options)
                }
            });
        },

        setDoneState: function(e){
            console.log('e', e, ' ', e.target, ' this.model  ', this.model, '  this.collection ', this.collection);
                var self = this;
                e.preventDefault();
                $(e.target)
                    .removeClass('ready')
                    .addClass('done');

            this.collection.fetch({
                success: function() {
                    var model = self.collection.get(self.id);

                    model.set({'status': 'done'});
                    model.save();
                    self.remove();

                    Events.socketEvents('doneMessage', {status: 'done', model: model})
                },
                error: function (collection, response, options) {
                    console.log('Error on fetch: ', collection, 'Error on fetch: ', response, 'Error on fetch: ', options)
                }
            })
        },

        deleteItem: function (e) {
            var self = this;
            console.log('e', e, ' this.model  ', this.model, '  this.collection ', this.collection);
            e.preventDefault();

            var dfd = $.Deferred();
            this.model.set({dfd: dfd});

            dfd
                .done(this.model.destroy(), function (res) {
                    console.log(res);
                    Events.trigger('assortment_item_deleted');
                });

            // $.when( this.model.destroy()) .then(function() {
            //     Events.trigger('assortment_item_deleted');
            // });

            //this.model.destroy();

            //this.model.collection.remove(this.model);
            this.remove();
            this.collection.remove(this.model);
        },

        render: function () {
            //var obj = Object.assign({}, this.options, {key: (this.options.id).slice(-4)});
            var context = {'data': this.options.attributes};
            var templateCompiled = Handlebars.compile(template);
            var html = templateCompiled(context);
            this.$el.html(html);
            this.afterRender();
            return this;
        },
        afterRender: function () {
            this.$el.find('.created_at').css('background', 'red');
        }
    });

    return TodoItemView;
});