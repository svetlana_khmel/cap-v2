define(['jquery', 'underscore', 'backbone',
    //'Handlebars',
    'vendor/handlebars-v4.0.5',
    'models/assortmentItemModel',
    'models/reportItemModel',
    'text!../../templates/check-view.html',
    'views/checkItemView',
    'collections/sellsCollection'
], function ($, _, Backbone, Handlebars, AssortmentItemModel, ReportItem, template, CheckItemView, SellsCollection) {

    var CheckViewContainer = Backbone.View.extend({
        el:'.js-check-container',
        tagName: 'div',
        // className: 'js-check-item',
        events: {
            'click .submit':'submitCheck'
        },
        initialize: function (options) {
            this.options = options || {};
            this.collection = this.options.collection;
            this.model = this.options.model;
        },
        submitCheck: function(e) {
            console.log("This collection: ", this.collection);

            var self = this;
            e.preventDefault();

            var data = JSON.parse(sessionStorage.getItem('storage'));
            console.log('CHECK....', data);
            var date = new Date();
            var created_at = date.getTime();

            var sellsCollection = new SellsCollection();

            _.each(data, function(modelData){
                var reportItem = new ReportItem(Object.assign({}, _.omit(modelData, '_id'), {'created_at': created_at}));
                reportItem.save();
                sellsCollection.add(reportItem);
            });

            this.remove();

            sessionStorage.clear();

            Backbone.router.navigate('statistic', {trigger: true});
        },
        render: function () {
            var self = this;
            self.renderItems();
            return this;
        },
        renderItems: function () {
            var self = this;
            var item = this.model;
            var stored = [];

            if(sessionStorage.getItem('storage')){
                stored = JSON.parse(sessionStorage.getItem('storage'));
                if(!_.contains(stored, item)) {
                    stored.push(item);
                    sessionStorage.setItem('storage', JSON.stringify(stored));
                }

            } else {
                stored.push(item);
                sessionStorage.setItem('storage', JSON.stringify(stored));
            }

            var checkItemView = new CheckItemView({model: self.model, collection: this.collection});
            $('.js-check').prepend(checkItemView.render().el);
        }
    });

    return CheckViewContainer;
});