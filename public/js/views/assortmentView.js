define([
    'text!../../templates/assortment-view.html',
    'views/baseView',
    'views/assortmentFormView',
    'views/userPanelView'
], function(template, BaseView, AssortmentFormView, UserPanelView){

    var AssortmentView = BaseView.extend({
        el: ".js-main-views",
        initialize: function (options) {
           BaseView.prototype.initialize.apply(this);
           this.options = options || {};
           this.el = $(this.options.parentEl).find(this.el);
        },
        beforeRender: function () {

        },
        render: function () {
            this.$el.html(template);
            return this;
        },
        afterRender: function() {
            console.log("AssortmentView afterRender ....");
            var assortmentFormView = new AssortmentFormView();
            assortmentFormView.render();
        }
    });

    return AssortmentView;
});