define([
    'vendor/handlebars-v4.0.5',
    'text!../../templates/day-statistic-view.html',
    'mixins/dates',
    'views/statisticBaseView'
], function(Handlebars, template, Dates, StatisticBaseView) {

    var DayStatisticView = StatisticBaseView.extend({
        el: ".js-day-statistic",
        initialize: function (options) {
            StatisticBaseView.prototype.initialize.apply(this, []);
            this.options = options || {};
        },
        rerenderReport: function (date) {
            this.date = date;

            this.$el.empty();
            this.render();
        },
        render: function(){

            var data = {date: this.date?this.date:Dates.getCurrentDate()};

            var compiledTemplate = Handlebars.compile(template);
            var html = compiledTemplate(data);
            this.$el.html(html);
            this.afterRender();
            return this;
        },
        afterRender: function() {
            var self = this;
            var user = localStorage.getItem('currentCupUser');
            var dayData = [];

            var datepicker = this.$el.find('.js-datepicker');

            datepicker.datepicker({
                dateFormat: 'dd/mm/yy',
                onSelect: function (date) {
                    console.log('Date selected: ', date);
                    self.rerenderReport(date);
                }
            });

            if (!this.date) {
                this.date = Dates.getCurrentDate();
                console.log('Date:::: ', this.date);
            }

            self.assortmentCollection.fetch().done(function(items){
                self.initHighchartDay(items, dayData);
            });

            self.sellsCollection.fetch().done(function(soldItems){
                //console.log("MODELS: ", models);
                console.log("soldItems: ", soldItems);

                _.each(soldItems, function(soldItem) {
                    //sort by user
                    if(soldItem.user === user) {

                        //sort by date
                        if (Dates.getItemDate(parseInt(soldItem.created_at)) === self.date ) {
                            dayData.push(soldItem);
                        }
                    }
                });

                self.assortmentCollection.fetch().done(function(items){
                    self.initHighchartDay(items, dayData);
                });
            });

        },

        initHighchartDay: function (assortment, data) {
            var categories = [];
            var obj = [];

            var summ = [];
            var quantity = [];

            _.each(assortment, function(item){
                categories.push(item.name);
            });

            _.each(assortment, function(item){
                obj.push({name: item.name, price: item.price});
            });

            _.each(obj, function(item){
                var itemsWasSoldArrayByName = _.where(data, {name: item.name});
                summ.push(itemsWasSoldArrayByName.length * item.price);
                quantity.push(itemsWasSoldArrayByName.length);
            });

            series = [{'name': 'Summ', data : summ}, {'name': 'Quantity', data : quantity}];

            $('.container-day-sells').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Продажи за день'
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    title: {
                        text: 'Продано'
                    }
                },
                series: series
//                series: [{
//                    name: 'Summ',
//                    data: [1, 0, 4]
//                }, {
//                    name: 'Qauntity',
//                    data: [5, 7, 3]
//                }]

            });

            this.initHightChartsByHours(assortment, data);
        },

        initHightChartsByHours: function (assortment, data) {
            var sellsByHours = [];

            _.each(assortment, function (item){
                sellsByHours.push(_.extend({}, {name: item.name, data:[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]}));
            });

            _.each(data, function (item){
               var date = new Date(parseInt(item.created_at));
               var hours = date.getHours();
               var arr  =  _.findWhere(sellsByHours, {name:item.name});
               arr.data[hours] ? arr.data[hours] += 1 : arr.data[hours] = 1;
            });

            console.log("______sellsByHours_ after add _____  ",  sellsByHours );

            $('.container-day-sells-by_hours').highcharts({

                chart: {
                    type: 'column'
                },

                title: {
                    text: 'Sells by hours'
                },

                subtitle: {
                    text: ''
                },

                legend: {
                    align: 'right',
                    verticalAlign: 'middle',
                    layout: 'vertical'
                },

                xAxis: {
                    categories: ['0:00', '1:00', '2:00', '3:00', '4:00', '5:00', '6:00', '7:00', '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00', '24:00'],
                    //categories: ['Apple ', 'Banana', 'Orange'],

                    labels: {
                        x: -10
                    }
                },

                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Amount'
                    }
                },

                // series: [{
                //     name: 'Capuchino',
                //     data: [1, 4, 3]
                // }, {
                //     name: 'Cofee',
                //     data: [6, 4, 2]
                // }, {
                //     name: 'Christmas Day after dinner',
                //     data: [8, 4, 3]
                // }],

                series: sellsByHours,

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                align: 'center',
                                verticalAlign: 'bottom',
                                layout: 'horizontal'
                            },
                            yAxis: {
                                labels: {
                                    align: 'left',
                                    x: 0,
                                    y: -5
                                },
                                title: {
                                    text: null
                                }
                            },
                            subtitle: {
                                text: null
                            },
                            credits: {
                                enabled: false
                            }
                        }
                    }]
                }
            });
        }
    });


    return DayStatisticView;
});
