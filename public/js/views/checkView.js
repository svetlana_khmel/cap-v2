define(['jquery', 'underscore', 'backbone',
    //'Handlebars',
    'vendor/handlebars-v4.0.5',
    'models/assortmentItemModel',
    'models/reportItemModel',
    'models/todoModel',
    'text!../../templates/check-view.html',
    'views/checkItemView',
    'views/reportView',
    'collections/sellsCollection',
    'collections/todoCollection',
    'mixins/events',
    'socketio'
], function ($, _, Backbone, Handlebars, AssortmentItemModel, ReportItem, TodoModel, template, CheckItemView, ReportView, SellsCollection, TodoCollection, events, io) {

    var CheckView = Backbone.View.extend({
        el:'.js-check-container',
        tagName: 'div',
        // className: 'js-check-item',
        events: {
            'click .submit':'submitCheck'
        },
        initialize: function (options) {
            this.options = options || {};
            this.collection = this.options.collection;
            this.model = this.options.model;
        },
        submitCheck: function(e) {
            var self = this;
            e.preventDefault();
            e.stopImmediatePropagation();

            var data = JSON.parse(sessionStorage.getItem('storage'));
            var date = new Date();
            var created_at = date.getTime();
            var user = localStorage.getItem('currentCupUser');

            var sellsCollection = new SellsCollection();

            //Create item for todo dashboard
            var todoModels = [];

            _.each(data, function(modelData){
                var reportItem = new ReportItem(Object.assign({}, _.omit(modelData, '_id'), {'created_at': created_at.toString(), 'user': user}));
                todoModels.push(reportItem);
                reportItem.save();

                sellsCollection.add(reportItem);
            });

            var todoItem = new TodoModel();

            todoItem.save({
                timeForOneItem: "", //In milliseconds
                status:"processing", // ready, done should be renamed to processing
                models: todoModels,
                user: user,
                created_at: created_at
            }, {
                success: function (res, model) {

                    console.log('RESPONSE: ', res);
                    console.log('MODEL: ', model, 'RES::: ', res);
                    var key = (model._id).slice(-4);
                    events.socketEvents('postMessage', key);
                    $('.js-last-sold-order').html(key);
                    //self.socketEvents('postMessage', key);

                    console.table(model);
                    console.dir(res);

                }.bind(this),
                error: function (model, res) {
                    console.log('Error ', model, res);
                    console.table('Error ', model);
                    console.dir('Error ', res);
                }.bind(this)
            });

            var todoCollection = new TodoCollection();
            todoCollection.add(todoItem);

            this.$el.removeClass('filled');
            this.$el.find('.js-check').empty();

            sessionStorage.clear();

            this.renderReport();
            return false;
        },
        socketEvents: function (event, data) {
           var socket = io();

           if(event === 'postMessage')
            socket.on('connect', function () {
                socket.emit('postMessage', {'data':data});
            });

            socket.on('updateMessage', function (data) {
                console.log("GOT DATA!!!  ", data);
            });
        },
        renderReport: function() {
            events.trigger('renderReport');
        },
        render: function () {
            var self = this;
            self.renderItems();
            return this;
        },
        renderItems: function () {
            var self = this;
            var item = this.model;
            var stored = [];

            if(sessionStorage.getItem('storage')){
                stored = JSON.parse(sessionStorage.getItem('storage'));
                if(!_.contains(stored, item)) {
                    stored.push(item);
                    sessionStorage.setItem('storage', JSON.stringify(stored));
                }

            } else {
                stored.push(item);
                sessionStorage.setItem('storage', JSON.stringify(stored));
            }

            var checkItemView = new CheckItemView({model: self.model, collection: this.collection});
            this.$el.find('.js-check').append(checkItemView.render().el);
            this.$el.hasClass('filled')? '' :this.$el.addClass('filled');
        }
    });

    return CheckView;
});