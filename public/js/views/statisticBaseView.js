define([
    'jquery','underscore', 'backbone',
    'jqueryui',
    'vendor/handlebars-v4.0.5',
    'collections/assortmentCollection',
    'collections/sellsCollection',
    'collections/reportCollection',
    'mixins/dates',
    'mixins/events',
    '../vendor/highcharts'

], function($, _, Backbone, ui, Handlebars, AssortmentCollection, SellsCollection, ReportCollection, Dates, events, highcharts) {

    var StatisticBaseView = Backbone.View.extend({
        initialize: function (options) {
            this.options = options || {};
            this.assortmentCollection = new AssortmentCollection();
            this.reportCollection = new ReportCollection();
            this.sellsCollection = new SellsCollection();
        }
        // render: function(){
        //
        //     var data = {date: 'today'};
        //
        //     var compiledTemplate = Handlebars.compile(template);
        //     var html = compiledTemplate(data);
        //     this.$el.html(html);
        //     return this;
        // }
    });


    return StatisticBaseView;
});
