define([
    'vendor/handlebars-v4.0.5',
    'text!../../templates/year-statistic-view.html',
    'mixins/dates',
    'views/statisticBaseView'
], function(Handlebars, template, Dates, StatisticBaseView) {

    var YearStatisticView = StatisticBaseView.extend({
        el: ".js-year-statistic",
        initialize: function (options) {
            StatisticBaseView.prototype.initialize.apply(this, []);
            this.options = options || {};


            this.assortmentCollection.fetch().done(function(items){
                //assortmentArr.push(item);
                // self.initHighchartDay(items, yearData);
                console.log("!!!!DayStatisticView  Test fetch ... ", items);
            });
        },
        events: {
            'click .js-month-list li':'chooseMonth'
        },
        chooseMonth: function (e) {
            var date = $(e.target).data('url') + '/' + Dates.getCurrentYear();
            this.rerenderReport(date)
        },
        rerenderReport: function (date) {
            this.date = date;

            this.$el.empty();
            this.render();
        },
        render: function(){

            var data = {date: this.date?this.date:Dates.getCurrentYear()};

            var compiledTemplate = Handlebars.compile(template);
            var html = compiledTemplate(data);
            this.$el.html(html);
            this.afterRender();
            return this;
        },
        afterRender: function() {
            var self = this;
            var user = localStorage.getItem('currentCupUser');
            var yearData = [];

            // var datepicker = this.$el.find('.js-datepicker');
            // datepicker.datepicker({
            //     onSelect: function (date) {
            //         console.log('Date selected: ', date);
            //         self.rerenderReport(date);
            //     }
            // });

            if (!this.date) {
                this.date = Dates.getCurrentYear();
                console.log('Date:::: ', this.date);
            }

            self.assortmentCollection.fetch().done(function(items) {
                self.initHighchartYear(items, yearData);
            });

            self.sellsCollection.fetch().done(function(soldItems) {
                //console.log("MODELS: ", models);
                console.log("soldItems: ", soldItems);

                _.each(soldItems, function(soldItem) {
                    //sort by user
                    if(soldItem.user === user) {

                        //sort by date
                        if (Dates.getItemDate(parseInt(soldItem.created_at), 'year') === self.date ) {
                            yearData.push(soldItem);
                        }
                    }
                });

                self.assortmentCollection.fetch().done(function(items){
                    //assortmentArr.push(item);
                    self.initHighchartYear(items, yearData);
                });
            });
        },

        initHighchartYear: function (assortment, data) {
            var dataByMonth = [];
            var summByMonth = [];

            // [
            //  [{}{}]
            //  [{}{}]
            //  [{}{}]
            // ]

            for (var i = 1; i <= 12; i ++) {
                var date = (i < 10 ? ('0'+i) : i)+ '/' + Dates.getCurrentYear();
                console.log('Date +++++ : ', date);
                var set = [];
                var setSumm = [];

                _.each(data, function (item) {
                    if(Dates.getItemDate(parseInt(item.created_at), 'month') === date){
                        set.push(item);
                        setSumm.push(item.price);
                    }
                });

                dataByMonth.push(set);
                summByMonth.push(setSumm);
            }

            var summ = [];
            var quantity = [];

            _.each(summByMonth, function(arr){
                quantity.push(arr.length);
                var sum = _.reduce(arr, function(memo, num){ return memo + num; }, 0);
                summ.push(sum);
            });

            $('.container-year-sells').highcharts({
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Monthly Average Sells'
                },
//                subtitle: {
//                    text: 'Source: WorldClimate.com'
//                },
                xAxis: {
                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                    title: {
                        text: 'Продажи'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [
                   {
                   name: 'Сумма',
                   data: summ
               },
                    {
                        name: 'Количество',
                        data: quantity
                    }]
            });

        }
    });


    return YearStatisticView;
});

