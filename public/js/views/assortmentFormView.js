define(['jquery', 'underscore', 'backbone',
        'text!../../templates/assortment-form.html',
        'models/assortmentItemModel',
        'collections/assortmentCollection',
        'views/assortmentItemView',
        'views/assortmentListView',
        'mixins/validation',
        'mixins/notifications',
        'mixins/events'
        ], function ($, _, Backbone, template, AssortmentItemModel, AssortmentCollection, AssortmentItemView, AssortmentListView, Validation, Notifications, Events) {

    var AssortmentFormView = Backbone.View.extend({
        model: new AssortmentItemModel(),
        el: '.js-assortment-form',
        events: {
            'submit .assortment-form': 'submit',
            'blur input': 'validation'
        },
        initialize: function (options) {
            this.option = options || {};
            var self = this;
            this.collection = new AssortmentCollection();

            this.collection.fetch({
                success: function() {
                    self.renderList(self.collection);
                },
                error: function (collection, response, options) {
                    console.log('Error on fetch: ', collection, 'Error on fetch: ', response, 'Error on fetch: ', options)
                }
            });

            Events.on('assortment_item_deleted', this.showNotification, this);
        },
        validation: function (e) {
            var fieldName = $(e.target).data('validation');
            var field = $(e.target);

            Validation.validate(fieldName, field, function (tooltip) {tooltip;}, function(status){return status;});
        },
        submit: function (e) {
            var self = this;
            e.preventDefault();

           if(Validation.validateForm(e.target) === false) return; //should return true

            //var data = JSON.parse(JSON.stringify($(e.target).serializeArray()));
            var array = $(e.target).serializeArray();
            this.form = $(e.target);

            var context = {
                name:"",
                price:"",
                type:""
            };

            array.forEach(function (item) {
                if(item.name === 'name') {
                    context.name = item.value;
                }
                if(item.name === 'price') {
                    context.price = item.value;
                }
            });

            context.user = localStorage.getItem('currentCupUser');

            var assortmentItem = new AssortmentItemModel(context);

            assortmentItem.save(null, {
                success: function (response) {
                    // Model.save() Ddoesn't have 'error' callback, and need set 'null' as first parameter to fire 'success'
                    console.log('Model has been saved successfully  ', response);
                    if(response.changed.errors) {
                        self.error = response.changed.errors.name.message;

                        Notifications.showNotification('error', self.error,  self.$el.find('.error-block'));

                    } else {
                        self.collection.add(assortmentItem);
                        self.renderList(self.collection);

                        self.form.find('input').each(function(index) {
                            $( this ).val('');
                        });

                        Notifications.showNotification('success', 'Saved succesfuly.',  self.$el.find('.error-block'));
                    }
                }
        });

            // self.collection.add(assortmentItem);
            // self.renderList(this.collection);
        },

        showNotification: function () {
            var self = this;
            Notifications.showNotification('success', 'Deleted succesfuly.',  self.$el.find('.error-block'));
        },

        renderList: function (data) {
            var assortmentListView = new AssortmentListView(data);
            assortmentListView.render();
        },

        render: function () {
            this.$el.html(template);
            return this;
        }
    });

    return AssortmentFormView;
});