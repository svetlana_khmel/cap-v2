define([
    'vendor/handlebars-v4.0.5',
    'text!../../templates/month-statistic-view.html',
    'mixins/dates',
    'views/statisticBaseView'
], function(Handlebars, template, Dates, StatisticBaseView) {

    var MonthStatisticView = StatisticBaseView.extend({
        el: ".js-month-statistic",
        initialize: function (options) {
            StatisticBaseView.prototype.initialize.apply(this, []);
            this.options = options || {};

            this.assortmentCollection.fetch().done(function(items){
                //assortmentArr.push(item);
                // self.initHighchartDay(items, dayData);
                console.log("!!!!DayStatisticView  Test fetch ... ", items);
            });
        },
        events: {
          'click .js-month-list li':'chooseMonth'
        },
        chooseMonth: function (e) {

            $(e.target).addClass('active');
            console.log('Choose month...', $(e.target));

            var date = $(e.target).data('url') + '/' + Dates.getCurrentYear();
            this.month = $(e.target).data('url');
            this.rerenderReport(date);
        },
        rerenderReport: function (date) {
            this.date = date;
            this.$el.empty();
            this.render();
        },
        render: function() {
            var months = [
                {
                    name: 'Jan',
                    num: '01',
                    active: ''
                },
                {
                    name: 'Feb',
                    num: '02',
                    active: ''
                },
                {
                    name: 'March',
                    num: '03',
                    active: ''
                },
                {
                    name: 'Apr',
                    num: '04',
                    active: ''
                },
                {
                    name: 'May',
                    num: '05',
                    active: ''
                },
                {
                    name: 'Jun',
                    num: '06',
                    active: ''
                },
                {
                    name: 'July',
                    num: '07',
                    active: ''
                },
                {
                    name: 'Aug',
                    num: '08',
                    active: ''
                },
                {
                    name: 'Sept',
                    num: '09',
                    active: ''
                },
                {
                    name: 'Oct',
                    num: '10',
                    active: ''
                },
                {
                    name: 'Nov',
                    num: '11',
                    active: ''
                },
                {
                    name: 'Dec',
                    num: '12',
                    active: ''
                }
            ];

            var active = _.findWhere(months, {num: this.month ? this.month : Dates.getCurrentMonthSlice()});
            active.active = 'active';

            var context = {data : months};

            var compiledTemplate = Handlebars.compile(template);
            var html = compiledTemplate(context);
            this.$el.html(html);
            this.afterRender();
            return this;
        },
        afterRender: function() {
            var self = this;
            var user = localStorage.getItem('currentCupUser');
            var monthData = [];

            // var datepicker = this.$el.find('.js-datepicker');
            // datepicker.datepicker({
            //     onSelect: function (date) {
            //         console.log('Date selected: ', date);
            //         self.rerenderReport(date);
            //     }
            // });

            if (!this.date) {
                this.date = Dates.getCurrentMonth();
                console.log('Date:::: ', this.date);
            }

            self.assortmentCollection.fetch().done(function(items) {
                self.initHighchartMonth(items, monthData);
            });

            self.sellsCollection.fetch().done(function(soldItems) {
                //console.log("MODELS: ", models);


                _.each(soldItems, function(soldItem) {
                    //sort by user
                    if(soldItem.user === user) {

                        //sort by date
                        if (Dates.getItemDate(parseInt(soldItem.created_at), 'month') === self.date ) {
                            monthData.push(soldItem);
                        }
                    }
                });

                self.assortmentCollection.fetch().done(function(items){
                    //assortmentArr.push(item);
                    self.initHighchartMonth(items, monthData);
                });
            });
        },

        initHighchartMonth: function (assortment, data) {
            var categories = [];
            var obj = [];
            var summ = [];
            var quantity = [];

            _.each(assortment, function(item){
                categories.push(item.name);
            });

            _.each(assortment, function(item){
                obj.push({name: item.name, price: item.price});
            });

            _.each(obj, function(item){
                var itemsWasSoldArrayByName = _.where(data, {name: item.name});
                summ.push(itemsWasSoldArrayByName.length * item.price);
                quantity.push(itemsWasSoldArrayByName.length);
            });

            series = [{'name': 'Summ', data : summ}, {'name': 'Quantity', data : quantity}];

            $('.container-month-sells').highcharts({
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Продажи за месяц'
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    title: {
                        text: 'Продано'
                    }
                },
                series: series
//                series: [{
//                    name: 'Summ',
//                    data: [1, 0, 4]
//                }, {
//                    name: 'Qauntity',
//                    data: [5, 7, 3]
//                }]

            });

        }
    });


    return MonthStatisticView;
});

