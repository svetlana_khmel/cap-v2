define([
    'jqueryui',
    'text!../../templates/report-view.html',
    'vendor/handlebars-v4.0.5',
    'models/reportItemModel',
    'collections/assortmentCollection',
    'collections/sellsCollection',
    'collections/reportCollection',
    'views/baseView',
    'views/reportItemView',
    'mixins/events',
    'mixins/dates'

], function(ui, template, Handlebars, ReportItemModel, AssortmentCollection, SellsCollection, ReportCollection, BaseView, ReportItemView, events, Dates){

    var ReportView = BaseView.extend({
        model: new ReportItemModel(),
        el: ".js-report-view",
        className: 'js-report',
        initialize: function (options) {
            this.options = options || {};
            //BaseView.prototype.initialize.apply(this);
            this.assortmentCollection = new AssortmentCollection();
            this.reportCollection = new ReportCollection();
            this.sellsCollection = new SellsCollection();

            events.on('renderReport', this.rerenderReport, this);

            $('.js-last-sold-order').on('click', function () {
                $(this).empty();
            })
        },

        rerenderReport: function (date) {
            if (date) {
                this.date = date;
            } else {
                this.date = Dates.getCurrentDate();
                // var date = new Date();
                // this.date = Dates.getItemDate(date.getTime());
            }
            this.$el.empty();
            this.render();
        },

        render: function () {
            var self = this;

            var data = {currentDate: self.date?self.date:Dates.getCurrentDate()};
            var html = Handlebars.compile(template);
            self.$el.append(html(data));
            this.afterRender(self.date);
            return this;
        },
        afterRender: function(date) {
            if (!date) {
                // var date = new Date();
                // this.date = Dates.getItemDate(date.getTime());
                // console.log('Date:::: ', this.date);
                this.date = Dates.getCurrentDate();
            }
            console.log("ReportView After render  ");
            var self = this;
            var user = localStorage.getItem('currentCupUser');
            var models = [];

            var datepicker = this.$el.find('.js-datepicker');
            datepicker.datepicker({
                onSelect: function (date) {
                    console.log('Date selected: ', date);
                    self.rerenderReport(date);
                }
            });

            this.assortmentCollection.fetch().done(function(items) {
               // console.log("+++  items ", items);

                _.each(items, function (item) {
                    //sort by user
                    if(item.user === user) {
                        models.push({'name': item.name, 'model': item, sold: 0});
                    }
                });

                self.sellsCollection.fetch().done(function(soldItems){
                    // console.log("MODELS: ", models);
                    // console.log("soldItems: ", soldItems);

                    _.each(soldItems, function(soldItem) {
                        _.each(models, function(model) {
                            if(model.name === soldItem.name) {
                                //sort by user
                                if(soldItem.user === user) {
                                    //sort by date
                                    if (Dates.getItemDate(parseInt(soldItem.created_at)) === self.date ) {
                                        model.sold += 1;
                                        // console.log("models.name ", model.name, "   soldItem.name ", soldItem.name);
                                    }
                                }

                            }
                        });
                    });

                    _.each(models, function(model) {
                        var reportItemModel = new ReportItemModel(model.model);
                        //reportItemModel.save();
                        self.reportCollection.add(reportItemModel);
                        //Child view
                        var reportItemView = new ReportItemView({model: reportItemModel, collection: self.reportCollection, sold: model.sold});

                        self.$el.find(".js-report-inner-container").append(reportItemView.render().el);
                    });

                   // console.log("----- ", models);

                });
            });
        },
        // getItemDate: function (time, format) {
        //     //Expects format mm/dd/yyyy
        //     var date = new Date(time); // Sat Feb 25 2017 18:31:10 GMT+0200 (EET)
        //     var year = date.getFullYear();
        //     var month = date.getMonth() + 1;
        //     month < 10? month = '0'+ month : month;
        //     var day = date.getDate();
        //     day < 10? day = '0'+ day : day;
        //     var fullDate = day +'/'+ month +'/'+ year;
        //
        //     switch(format){
        //         case 'year':
        //             return year;
        //
        //
        //         default:
        //             return fullDate;
        //
        //     };
        //
        // }
    });

    return ReportView;
});