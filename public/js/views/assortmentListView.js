define([
    'views/baseView',
    'views/assortmentItemView',
    'text!../../templates/assortment-list.html',
    'models/assortmentItemModel'
], function (BaseView, AssortmentItemView, template, AssortmentItemModel, AssortmentCollection) {
    var AssortmentListView = BaseView.extend({
        model: new AssortmentItemModel(),
        el: '.js-assortment-list',
        initialize: function (options) {
            this.options = options || {};
            console.log('List view ... ', this.options);
        },
        beforeRender: function () {
            console.log('*** before RENDER::::    ');
        },
        render: function () {
           this.$el.html(template);
           this.afterRender();
           return this;
        },
        afterRender: function () {
            var self = this;
            this.options.models.forEach(function (item) {
                self.renderItemList(item);
            });
        },
        renderItemList: function (item) {
            var user = localStorage.getItem('currentCupUser');
            console.log('*** AFTR RENDER::::    ');
            if(item.get('user') === user) {
                var itemView = new AssortmentItemView({model:item});
                this.$el.find('.js-assortment-inner-container').append(itemView.render().el);
            }
        }
    });

    return AssortmentListView;
});