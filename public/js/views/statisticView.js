define([
    'jqueryui',
    'text!../../templates/statistic-view.html',
    'vendor/handlebars-v4.0.5',
    'views/baseView',
    'views/dayStatisticView',
    'views/monthStatisticView',
    'views/yearStatisticView'
], function(ui, template, Handlebars, BaseView, DayStatisticView, MonthStatisticView, YearStatisticView){

    var StatisticView = BaseView.extend({

        el: ".js-main-views",
        initialize: function (options) {
            this.options = options || {};
        },

        render: function () {
            this.$el.html(template);
            this.afterRender();
            return this;
        },
        afterRender: function() {

            var dayStatisticView = new DayStatisticView();
            dayStatisticView.render();

            var monthStatisticView = new MonthStatisticView();
            monthStatisticView.render();

            var yearStatisticView = new YearStatisticView();
            yearStatisticView.render();

        }
    });

    return StatisticView;
});