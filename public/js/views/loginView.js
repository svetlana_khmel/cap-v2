define([
    'vendor/handlebars-v4.0.5',
    //'handlebars',
    'text!../../templates/login-form.html',
    'models/user',
    'views/baseView',
    'vendor/md5',
    'mixins/validation',
    'mixins/notifications'
], function(Handlebars, template, UserModel,  BaseView, md5, Validation, Notifications){

    var LoginView = BaseView.extend({
        el: ".container",
        events: {
           'submit .login-form': 'loginSubmit',
           'submit .registration-form': 'registration',
           'blur input': 'validation',
           'click .gallery-bullets li': 'galleryNavigate',
           'click .demo .next': 'goNext',
           'click .demo .back': 'goBack'
        },
        initialize: function (options) {
            BaseView.prototype.initialize.apply(this);
            this.options = options || {};
            this.el = $(this.options.parentEl).find(this.el);

        },
        galleryNavigate: function (e) {
            console.log($(e.target).index());
            var index = $(e.target).index();

            this.$el.find('.gallery li').removeClass('active');
            $(this.$el.find('.gallery li')[index]).addClass('active');

            this.$el.find('.gallery-bullets li').removeClass('active');
            $(this.$el.find('.gallery-bullets li')[index]).addClass('active');
        },
        goNext: function (e) {
            console.log($(e.target).index());
            var index = this.$el.find('.gallery .active').index();

            var $gallery = this.$el.find('.gallery');

            if(index < $gallery.find('li').length - 1) {
                this.$el.find('.gallery .active').removeClass('active');
                $(this.$el.find('.gallery li')[index + 1]).addClass('active');

                this.$el.find('.gallery-bullets li').removeClass('active');
                $(this.$el.find('.gallery-bullets li')[index + 1]).addClass('active');
            }
        },
        goBack: function (e) {
                    console.log($(e.target).index());
                    var index = this.$el.find('.gallery .active').index();

                    var $gallery = this.$el.find('.gallery');

                    if(index != 0) {
                        this.$el.find('.gallery li').removeClass('active');
                        $(this.$el.find('.gallery li')[index - 1]).addClass('active');

                        this.$el.find('.gallery-bullets li').removeClass('active');
                        $(this.$el.find('.gallery-bullets li')[index - 1]).addClass('active');
                    }
        },

        loginSubmit: function (e) {
            var self = this;
            e.preventDefault();

            if(Validation.validateForm(e.target) === false) return; //should return true

            //var data = JSON.parse(JSON.stringify($(e.target).serializeArray()));
            var array = $(e.target).serializeArray();

            var context = {
                username:"",
                password:""
            };

            array.forEach(function (item) {
                if(item.name === 'username') {
                    context.username = item.value;
                }
                if(item.name === 'password') {
                    context.password = md5(item.value);
                }
            });

            var dfd = $.Deferred();
            context.dfd = dfd;
            var user = new UserModel(context);

            dfd
                .done(user.logIn(), function (res){
                    console.log('Done ', res);
                    if(res.status === 401) {
                        this.errorProcessing(res, 'login');
                    }
                    if(res._id) {
                        this.startApplication(res);
                    }

                }.bind(this))
                .fail(function(error){
                    console.log('Failed ', error);
                }.bind(this));
        },
        validation: function (e) {
            var fieldName = $(e.target).data('validation');
            var field = $(e.target);

            Validation.validate(fieldName, field, function (tooltip) {tooltip;}, function(status){return status;});
        },
        setCurrentUser: function (user) {
            localStorage.setItem('currentCupUser', user);
        },
        registration: function (e){
            console.log('Clickkk1');
            var self = this;
            e.preventDefault();

            if(Validation.validateForm(e.target) === false) return; //should return true

            //e.stopImmediatePropagation();
            //var data = JSON.parse(JSON.stringify($(e.target).serializeArray()));
            var array = $(e.target).serializeArray();

            var context = {
                username:"",
                password:""
            };

            array.forEach(function (item) {
                if(item.name === 'username') {
                    context.username = item.value;
                }
                if(item.name === 'password') {
                    context.password = md5(item.value);
                }
            });

            console.log('Register context. . ... ', context);
            var user = new UserModel();

            user.save(context, {
                success: function (res, model) {
                    if(res.changed.errors) {
                        self.error = res.changed.errors.username.message;
                        Notifications.showNotification('error', self.error,  self.$el.find('.error-block'));

                    } else {
                        this.setCurrentUser(res.username);
                        Backbone.router.navigate('assortment', {trigger: true});
                    }
                    console.log(model, res);
                    console.table(model);
                    console.dir(res);


                }.bind(this),
                error: function (model, res) {
                    console.log('Error ', model, res);
                    console.table('Error ', model);
                    console.dir('Error ', res);
                    this.errorProcessing(res, 'registration');
                }.bind(this)
            });
        },
        startApplication: function (res) {
            this.setCurrentUser(res.username);
            Backbone.router.navigate('assortment', {trigger: true});
        },
        errorProcessing: function (error, form) {
            if (error.status === 401){
                var errorMessage = '\n' +'Sorry, user with such credentials doesn\'t exist:( Please, check your credentials or register as a new user.'
                this.data = error.statusText + ', ' + error.status + errorMessage;
            }

            if (error.status === 500){
                var errorMessage = '\n' +'Sorry, username is taken. Please, choose an uniq username.';
                this.data = error.statusText + ', ' + error.status + errorMessage;
            }

            if (form === 'registration') {
                this.data = {registerError: this.data}
            }

            if (form === 'login') {
                this.data = {loginError:  this.data}
            }
            this.render();
        },
        beforeRender: function () {

        },
        render: function () {
            var data = this.data;
            var templateCompiled = Handlebars.compile(template);
            var html = templateCompiled(data);

            this.$el.html(html);
            return this;
        },
        afterRender: function() {
            //.bindAll(this, this.events);
           console.log("LoginView....... afterRender ....", this.$el.find('.register-submit'), this.events);
           this.trigger('registerSubmit');
            // console.log("AssortmentView afterRender ....");
            // var assortmentFormView = new AssortmentFormView();
            // assortmentFormView.render();
        }
    });

    return LoginView;
});