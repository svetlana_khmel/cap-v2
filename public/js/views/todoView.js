define(['jquery', 'underscore', 'backbone',
    'text!../../templates/todo-view.html',
    'models/todoModel',
    'collections/todoCollection',
    'views/todoItemView',
    'mixins/validation',
    'mixins/notifications',
    'mixins/events'
], function ($, _, Backbone, template, TodoModel, TodoCollection, TodoItemView, Validation, Notifications, Events) {

    var TodoView = Backbone.View.extend({
        model: new TodoModel(),
        el: '.js-todo-view',
        initialize: function (options) {
            this.option = options || {};
            var self = this;
            this.collection = new TodoCollection();

            this.collection.fetch({
                success: function() {
                    self.renderList(self.collection);
                },
                error: function (collection, response, options) {
                    console.log('Error on fetch: ', collection, 'Error on fetch: ', response, 'Error on fetch: ', options)
                }
            });

            //Events.on('assortment_item_deleted', this.showNotification, this);
        },

        showNotification: function () {
            var self = this;
            Notifications.showNotification('success', 'Deleted succesfuly.',  self.$el.find('.error-block'));
        },

        renderList: function (data) {

            _.each(data.models, function (model) {
               if(model.get('status') !== 'done') {
                   if(!model.get('key')) {
                       model.set({'key': (model.get('_id')).slice(-4)});
                       model.save();
                   }
                   var todoItemView = new TodoItemView(model);
                   this.$el.append(todoItemView.render().el);
               }

            }.bind(this));

        },

        render: function () {
            this.$el.html(template);
            return this;
        }
    });

    return TodoView;
});