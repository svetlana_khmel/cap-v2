define(['views/baseView',
        'text!../../templates/layout.html',
        'views/assortmentView',
        'views/reportView',
        'views/statisticView',
        'views/loginView',
        'views/todoView',
        'mixins/checkStorage',
        'mixins/events'

], function (BaseView, template, AssortmentView, ReportView, StatisticView, LoginView, TodoView, CheckStorage, events) {

    var AppView = BaseView.extend({
        el: '.container',
        events: {
            'click .js-navigation li':'onClick'
        },
        initialize: function(options) {
            this.options = options || {};
            _.extend(BaseView.prototype, CheckStorage);
            BaseView.prototype.initialize.apply(this);

            this.clearSessionStorage();

            // events.on('renderReport', this.rerenderReport, this);
        },
        onClick: function (e) {
            e.preventDefault();
            console.log('Event', e.target);
           // this.$el.find('.js-navigation li').removeClass('active');
            var $li = $(e.target);
            //$li.addClass('active');
            Backbone.router.navigate($li.attr('data-url'), {trigger: true});
        },
        rerenderReport: function () {
            this.options.pageToLoad = 'report';
            this.afterRender();
        },
        beforeRender: function() {
            console.log('beforeRender');
        },
        render: function () {
            this.$el.html(template);
            return this;
        },
        afterRender: function() {
            console.log('afterRender');

            if (this.options.pageToLoad === 'login') {
                var loginView  = new LoginView({parentEl: this.el});
                loginView.render();
            }

            if (this.options.pageToLoad === 'assortment') {
                var assortmentView  = new AssortmentView({parentEl: this.el});
                assortmentView.render();
            }

            if (this.options.pageToLoad === 'report') {
                //var reportView  = new ReportView({parentEl: this.el});
                var reportView  = new ReportView({parentEl: this.el});
                reportView.render();
            }

            if (this.options.pageToLoad === 'statistic') {
                var statisticView  = new StatisticView({parentEl: this.el});
                statisticView.render();
            }

            if (this.options.pageToLoad === 'todo') {
                var todoView  = new TodoView({parentEl: this.el});
                todoView.render();
            }

            var active =  this.$el.find("[data-url='" + this.options.pageToLoad + "']");
            $(active).addClass('active');
        }
    });

    return AppView;
});