define([
    'underscore',
    'backbone',
    // Pull in the Model module from above
    'models/assortmentItemModel'
], function(_, Backbone, AssortmentItemModel){
    var AssortmentCollection = Backbone.Collection.extend({
        model: AssortmentItemModel,
        url: '/api/assortment',
        //url: '../data/data.json',
        //comparator: function(id) {
            //return page.get('ordering');
        //},

        initialize: function () {
            var self = this;

            if(this.get('parent') !== ''){
                self.set('type', 'coffee');
            } else {
                self.set('type', 'other');
            }

            // This will be called when an item is added. pushed or unshifted
            this.on('add', function(model) {
                //console.log('something got added');
               // this.sync('create');
                //self.syncr('add');
            });

            this.on('remove', function(options) {
               // console.log('something got removed', options);
               // this.sync('create');
                //self.syncr('add');
            });

            // This will be called when an item is removed, popped or shifted
            // this.on('remove',  function(model) {
            //     console.log('something got removed');
            // });
            // This will be called when an item is updated
            // this.on('change', function(model) {
            //     console.log('something got changed');
            // });
        },

        comparator: function(item) {
            return item.get('name')
        },

        syncr: function (option) {
            if(option === 'add') {
                var jsonCollection = JSON.stringify(this.models);
                localStorage.setItem('assortmentCollection', jsonCollection);
            }
        },

        // sync: function (options) {
        //     var self = this;
        //
        //     var user = localStorage.getItem('currentCupUser');
        //
        //     $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        //         jqXHR.setRequestHeader("cookie",user);
        //         jqXHR.setRequestHeader("Set-Cookie","session="+user);
        //
        //         // if (originalOptions.type === 'PUT')  {
        //         //     options.url = originalOptions.url + '/' + self.id
        //         // }
        //         console.log('options   , ', options, '   originalOptions,  ', originalOptions);
        //     });
        //
        //     return Backbone.sync.apply(this, arguments);
        // }

        // sync: function (options) {
        //     var self = this;
        //
        //     $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
        //         options.url =  self.url;
        //     });
        //
        //     console.log('Sync .__________!!!!!.......... ', options);
        //     return Backbone.sync.apply(this, arguments);
        // }

        // sync: function (options) {
        //     console.log('Sync .... ', options);
        //
        //     if(options === 'update') {
        //         var newModel =  JSON.stringify(this.models);
        //
        //         if(localStorage.getItem('assortmentCollection')){
        //             var data = JSON.parse(localStorage.getItem('assortmentCollection'));
        //             console.log('___ ', data);
        //             var promise = new Promise(function (resolve, reject) {
        //                 // return JSON.parse(localStorage.getItem('assortmentCollection'));
        //                 return resolve(_.union(data, newModel));
        //             });
        //         } else {
        //             localStorage.setItem('assortmentCollection', newModel);
        //         }
        //         //this.url='';
        //         //return Backbone.sync.apply(this, arguments);
        //     }
        //
        //
        //     // if(localStorage.getItem('assortmentCollection')){
        //     //     var data = JSON.parse(localStorage.getItem('assortmentCollection'));
        //     //     console.log('___ ', data);
        //     //     var promise = new Promise(function (resolve, reject) {
        //     //         // return JSON.parse(localStorage.getItem('assortmentCollection'));
        //     //         resolve(data);
        //     //     });
        //     // }
        //     // if(options === 'create') {
        //     //     var jsonCollection = JSON.stringify(this.models);
        //     //     localStorage.setItem('assortmentCollection', jsonCollection);
        //     //     ///return Backbone.sync.apply(this, arguments);
        //     // }
        //
        //     if(options === 'read') {
        //         if(localStorage.getItem('assortmentCollection')){
        //             var data = JSON.parse(localStorage.getItem('assortmentCollection'));
        //             console.log('___ ', data);
        //             var promise = new Promise(function (resolve, reject) {
        //                // return JSON.parse(localStorage.getItem('assortmentCollection'));
        //                 return resolve(data);
        //             });
        //         }
        //     }
        //     //return Backbone.sync.apply(this, arguments);
        //     //Backbone.sync.apply(this, arguments);
        // }
    });
    // You don't usually return a collection instantiated
    return AssortmentCollection;
});