define([
    'underscore',
    'backbone',
    'models/reportItemModel'
], function(_, Backbone, ReportItemModel){
    var reportCollection = Backbone.Collection.extend({
        model: ReportItemModel,
        url: '/api/report',
        //comparator: function(id) {
        //return page.get('ordering');
        //},
        initialize: function () {
            // This will be called when an item is added. pushed or unshifted
            this.on('add', function(model) {
                console.log('SellsCollection something got added');
            });

            this.on('remove', function(options) {
                console.log('SellsCollection something got removed', options);
            });
        },

        comparator: function(item) {
            return item.get('name')
        },

        // sync: function (options) {
        //     var self = this;
        //
        //     $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
        //         options.url =  self.url;
        //     });
        //
        //     return Backbone.sync.apply(this, arguments);
        // }
    });

    return reportCollection;
});