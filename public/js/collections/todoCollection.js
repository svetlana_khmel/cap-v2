define([
    'underscore',
    'backbone',
    'models/todoModel'
], function(_, Backbone, TodoModel){
    var todoCollection = Backbone.Collection.extend({
        model: TodoModel,
        url: '/api/todo',

        // initialize: function () {
        //     // This will be called when an item is added. pushed or unshifted
        //     this.on('add', function(model) {
        //         console.log('SellsCollection something got added');
        //     });
        //
        //     this.on('remove', function(options) {
        //         console.log('SellsCollection something got removed', options);
        //     });
        // },



        comparator: function(item) {
            return item.get('name')
        }
    });

    return todoCollection;
});