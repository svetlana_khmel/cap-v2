define([
    'underscore',
    'backbone',
    'models/reportItemModel'
], function(_, Backbone, ReportItemModel){
    var checkCollection = Backbone.Collection.extend({
        model: ReportItemModel,
        //comparator: function(id) {
        //return page.get('ordering');
        //},
        initialize: function () {

        },

        // sync: function (options) {
        //     var self = this;
        //
        //     // $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
        //     //     options.url =  self.url;
        //     // });
        //
        //     return Backbone.sync.apply(this, arguments);
        // },

        comparator: function(item) {
            return item.get('name')
        }
    });

    return checkCollection;
});