define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var ReportItemModel = Backbone.Model.extend({
        idAttribute: "_id",
        defaults: {
            // _id: "",
            name: "",
            price:"",
            type: "",
            user:"",
            created_at: ""
        },

        //urlRoot: "/api/assortment",
        url: "/api/report",

        validate: function(attr) {
            // if(!attr.name) {
            //     console.log('Price is required');
            //     return "Price is required."
            // }
            // if(!attr.price) {
            //     return "Price is required."
            // }
        }
        //,

        // sync: function (option) {
        //     var self =  this;
        //
        //     //if (option === 'create' || option === 'update') {
        //
        //     // var jsonCollection = products.toJSON();
        //     // localStorage.setItem('key', jsonCollection);
        //     console.log("_______ SYNC MODEL _________   ", option, '  ... ', this);
        //
        //     //localStorage.setItem('myCat', 'Tom');
        //     //"create", "read", "update", or "delete")
        //     //} else {
        //     return Backbone.sync.apply(this, arguments);
        //     //}
        // }
    });
    // Return the model for the module
    return ReportItemModel;
});