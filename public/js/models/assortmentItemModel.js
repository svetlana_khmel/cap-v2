define([
    'underscore',
    'backbone'
], function(_, Backbone){
    var AssortmentItemModel = Backbone.Model.extend({
        idAttribute: "dataId",
        defaults: {
           // _id: "",
            name: "",
            price:"",
            type: "",
            user: ""
        },

        //urlRoot: "/api/assortment",
        url: "/api/assortment",

        validate: function(attr) {
            if(!attr.name) {
                console.log('Price is required');
                return "Price is required."
            }
            if(!attr.price) {
                return "Price is required."
            }
        },

        destroy: function () {
            this.sync('delete', this, this.collection);
        },

        sync: function (options) {
            var self =  this;

            console.log('options ', options);
            // $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
            //     options.url =  self.url;
            //     options.data = options.data;
            // });

            if (options === 'delete') {

                // $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
                //     options.url =  self.url + '/' + self.get('_id');
                //     options.data = self.get('_id');
                // });
                var dfd = self.get('dfd');

                $.ajax({
                    url: self.url + '/' + self.get('_id'),
                    type: 'DELETE',
                    success: function( data, textStatus, jqXHR ) {
                        dfd.resolve('done!!!!');
                        console.log( 'Delete response:' );
                        console.dir( data );
                        console.log( textStatus );
                        console.dir( jqXHR );
                    }
                });
            }

            // console.log('..... ', self.url + self.get('_id'));
            // if (options === 'delete') {
            //     // $.ajaxSetup({
            //     //     url: self.url + '/' + self.get('_id'),
            //     //     method: 'DELETE'
            //     // });
            //
            //     $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
            //         options.url =  self.url + '/' + self.get('_id');
            //         options.method = 'DELETE';
            //         // Modify options, control originalOptions, store jqXHR, etc
            //     });
            //
            //    // self.url = self.url  + '/' +  self.get('_id');
            // }

            //if (option === 'create' || option === 'update') {

                // var jsonCollection = products.toJSON();
                // localStorage.setItem('key', jsonCollection);

                //localStorage.setItem('myCat', 'Tom');
                //"create", "read", "update", or "delete")
            //} else {
                return Backbone.sync.apply(this, arguments);
            //}
        }
    });
    // Return the model for the module
    return AssortmentItemModel;
});