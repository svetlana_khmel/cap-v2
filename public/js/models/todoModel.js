define(['jquery', 'underscore', 'backbone'], function($, _, Backbone) {
    var todoModel = Backbone.Model.extend({
        idAttribute: "_id",
        defaults: {
            // _id: "",
            timeForOneItem: "", //In milliseconds
            status:"", // ready
            key:"",
            models: [],
            user:"",
            created_at: ""
        },

        url: "/api/todo",

        sync: function (options) {
            var self = this;


            $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
                if (originalOptions.type === 'PUT')  {
                    options.url = originalOptions.url + '/' + self.id
                }

            });

            return Backbone.sync.apply(this, arguments);
        }
    });

    return todoModel
});