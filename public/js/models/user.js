define([], function () {
    var Model = Backbone.Model.extend({
        idAttribute: "_id",
        defaults: {
            // _id: "",
            username: "",
            password:""
        },

        //urlRoot: "/api/assortment",
        url: "/api/users",

        validate: function(attr) {
            // if(!attr.name) {
            //     console.log('Price is required');
            //     return "Price is required."
            // }
            // if(!attr.price) {
            //     return "Price is required."
            // }
        },

        logIn: function () {
            var dfd = this.get('dfd');

            $.post(this.url + /login/, {'username': this.get('username'), 'password': this.get('password')} , function (data, textStatus, jqXHR) {
                console.log("Get response:");
                console.dir("Data: ", data);
                console.log("textStatus: ", textStatus);
                console.log("Get response:", jqXHR);
                dfd.resolve(data);
            }.bind(this))
                // .done(function(){
                //     dfd.resolve(data);
                // })
            .fail(function(data, textStatus, jqXHR){
                console.log("Get response:");
                console.dir("Data: ", data);
                console.log("textStatus: ", textStatus);
                console.log("Get response:", jqXHR);
                dfd.resolve(data);
            }.bind(this));
        },

        // sync: function (options) {
        //     console.log(options);
        //
        //     if (options === 'create') {
        //         console.log('@...', options);
        //     }
        //     if (options === 'read') {
        //         console.log('2@...', options);
        //     }
        //
        //     return Backbone.sync.apply(this, arguments);
        // }
    });
    return Model;
});