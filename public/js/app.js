define(['jquery', 'underscore', 'backbone', 'router', 'global'], function($, _, Backbone, Router, global){
    var initialize = function () {
        var router = new Router();

        global.router = router;
        Backbone.router = router;

        Backbone.history.start({pushState:true});
    };

    return {
        initialize: initialize
    }

});
