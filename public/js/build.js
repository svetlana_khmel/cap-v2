requirejs.config({
    baseUrl: 'js',
    // To get timely, correct error triggers in IE, force a define/shim exports check.
    enforceDefine: false,
    shim: {
        underscore: '_',
        global: 'global'
    },
    backbone: {
        deps: [
            'underscore',
            'jquery'
        ],
        exports: 'Backbone'
    },
    jquery: {
        exports: '$'
    },
    jqueryui: {
        exports: '$',
        deps: ['jquery']
    },
    highcharts: {
        exports: 'Highcharts',
        deps: ["jquery"]
    },
    bootstrap : {
        deps :['jquery'],
        export: 'bootstrap'
    },
    handlebars: {
        'exports': 'handlebars'
    },
    'socketio': {
        exports: 'io'
    },
    paths: {
        jquery: 'vendor/jquery-3.0.0.min',
        jqueryui: 'vendor/jquery-ui',
        //underscore: 'vendor/underscore-min',
        underscore: 'vendor/underscore',
        //backbone: 'vendor/backbone-min',
        backbone: 'vendor/backbone',
        bootstrap: 'libs/bootstrap.min',
        Highcharts: 'vendor/highcharts',
        text: "vendor/text",
        handlebars: 'vendor/handlebars-v4.0.5',
        socketio: 'vendor/socket.io',
        global: './global'
    },
    packages: [{
        name: 'moment',
        // This location is relative to baseUrl. Choose bower_components
        // or node_modules, depending on how moment was installed.
        location: 'libs/',
        main: 'moment-with-locales'
    }]
});

//require([views/Appview]);
require([
    'app'
], function(App){
    App.initialize();
});