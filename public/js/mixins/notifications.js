define(['jquery'], function ($) {
    return notifications = {
        showNotification: function (status, message, el) {
           var self = this;
           this.el = el;

            if (status === 'success') {
               $(el)
                   .addClass('success')
                   .html(message);

               setTimeout(self.hideMessage.bind(this), 3000);
           }

           if (status === 'error') {
               $(el)
                   .addClass('error')
                   .html(message);
           }

        },

        hideMessage: function () {
            $(this.el)
                .empty()
                .removeClass('success')
                .removeClass('error');
        }
    }
});