define(['jquery'], function($){
    return checkStorage = {
        el: '',
        clearSessionStorage: function(item) {
            console.log("Session storage item has been cleared.");
            sessionStorage.clear();
        },

        add: function (item) {
            console.log("Session storage item has been added.");
            sessionStorage.setItem('','');
        },

        remove: function (item) {
            console.log("Session storage item has been removed.");
            sessionStorage.setItem('','');
        },
        test: function () {
            console.log('CheckStorage test ... ');
        }
    }
});