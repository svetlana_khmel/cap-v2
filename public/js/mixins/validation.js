define(['jquery'], function($){
    //Form should contain <fieldset>
    //Input should have data-validation attribute
    //After form should be <div class="tooltip error"></div>

    return validation = {
        rules: [
            {
                name: 'username',
                pattern: [/^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/],
                messages: {
                    required: "This field can\'t be empty",
                    positive: "",
                    negative: 'Usernames can consist of lowercase and capitals. Usernames can consist of alphanumeric characters. Usernames can consist of underscore and hyphens and spaces. Cannot be two underscores, two hypens or two spaces in a row. Cannot have a underscore, hypen or space at the start or end.'
                },
                min: 4,
                max: 40,
                required: true
            },
            {
                name: 'password',
                pattern: [/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/],
                messages: {
                    required: "This field can\'t be empty",
                    positive: "",
                    negative: "Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number",
                },
                min: 4,
                max: 40,
                required: true
            },
            {
                name: 'email',
                type: 'email',
                pattern: [/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/],
                messages: {
                    positive: "",
                    negative: "Don't forget @."
                },
                min: 4,
                max: 40,
                required: false
            },
            {
                name: 'assortment-name',
                pattern: [/^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$/],
                messages: {
                    required: "This field can\'t be empty",
                    positive: "",
                    negative: 'Usernames can consist of lowercase and capitals. Usernames can consist of alphanumeric characters. Usernames can consist of underscore and hyphens and spaces. Cannot be two underscores, two hypens or two spaces in a row. Cannot have a underscore, hypen or space at the start or end.'
                },
                min: 4,
                max: 40,
                required: true
            },
            {
                name: 'price',
                pattern: [/^\d+(.\d{1,2})?$/],
                messages: {
                    required: "This field can\'t be empty",
                    positive: "",
                    negative: 'Should be in XX.XX format'
                },
                min: 4,
                max: 40,
                required: true
            }
        ],

        validate: function (fieldName, field, tooltip, status) {
           _.each(this.rules, function (rule) {

               if(rule.name === fieldName) {
                   console.log('Rule was find ', rule);
                   console.log('Field ', $(field).val());
                   var regexp = rule.pattern[0];
                   if ($(field).val() === '' && rule.required) {
                       console.log('Field validation failed: ', rule.messages.required);
                       tooltip($(field)
                           .closest('fieldset').find('.tooltip').html(rule.messages.required));
                       status(false);

                   } else if(! regexp.test($(field).val())) {
                       tooltip($(field)
                           .closest('fieldset').find('.tooltip').html(rule.messages.negative));
                       status(false);
                   } else {
                       tooltip($(field)
                           .closest('fieldset').find('.tooltip').empty());
                       status(true);
                   }
               }
           });
        },

        validateForm: function (form) {
            var validatedFields = [];

           _.each($(form).find('input'), function (input){
               console.log('Form ', input.dataset.validation);
               var toValidate = this.validate(input.dataset.validation, input, function (tooltip) {tooltip;}, function(status){
                   validatedFields.push(status);
               });

           }.bind(this));

           if(_.contains(validatedFields, false )){
               return false;
           } else {
               return true;
           }
        }
    }
});