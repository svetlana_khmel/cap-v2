define(['socketio'], function(io){


   //var events = _.extend({}, Backbone.Events);
    var eventObj = {
        socketEvents: function (event, data) {
            var socket = io();

            if(event === 'postMessage') {
                socket.emit('postMessage', {'data':data});
            }

            if(event === 'doneMessage') {
                socket.emit('doneMessage', {'data':data});
            }

             // socket.on('connect', function () {
             //     socket.emit('postMessage', {'data':data});
             // });

             // socket.on('postMessage', function () {
             //     socket.emit('postMessage', {'data':data});
             // });


             // Uncomment this if you whant response message
             // socket.on('updateMessage', function (data) {
             //     console.log("GOT DATA on event mixin!!!  ", data);
             // });

        }
    };

    return events = _.extend(eventObj, Backbone.Events);
});