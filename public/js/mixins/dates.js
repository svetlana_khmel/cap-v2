define([], function () {
    return dates = {

        getCurrentDate: function() {
            var date = new Date();
            return this.getItemDate(date.getTime());
        },

        getCurrentMonthSlice: function() {
            var date = new Date();
            return this.getItemDate(date.getTime(), 'monthSlice');
        },

        getCurrentMonth: function() {
            var date = new Date();
            return this.getItemDate(date.getTime(), 'month');
        },

        getCurrentYear: function() {
            var date = new Date();
            return this.getItemDate(date.getTime(), 'year');
        },

        getItemDate: function (time, format) {
            //Expects format mm/dd/yyyy
            var date = new Date(time); // Sat Feb 25 2017 18:31:10 GMT+0200 (EET)
            var year = date.getFullYear();
            var month = date.getMonth() + 1;
            month < 10? month = '0'+ month : month;
            var day = date.getDate();
            day < 10? day = '0'+ day : day;
            var fullDate = day +'/'+ month +'/'+ year;

            switch(format){
                case 'year':
                    return year;
                case 'month':
                    return month + '/' + year;
                case 'monthSlice':
                    return month;
                default:
                    return fullDate;

            }
        }
    };
});