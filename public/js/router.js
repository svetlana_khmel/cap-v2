define(['jquery',
    'underscore',
    'backbone',
    'views/appView',
    'views/starterView',
    'views/assortmentView',
    'views/reportView',
    'views/statisticView',
    'views/loginView',
    'views/userPanelView'

], function ($, _, Backbone, AppView, StarterView, AssortmentView, ReportView, StatisticView, LoginView, UserPanelView) {

    var AppRouter = Backbone.Router.extend({
        routes: {
            ''     : 'index',
            'login': 'login',
            'logout': 'logout',
            'assortment': 'assortment',
            'goods:goodsId': 'viewGoodsById',
            'report':'report',
            'statistic':'statistic',
            'todo':'todo',

            '*other':'defaultRoute'
        },

        initialize: function () {
            this.pageToLoad = '';
        },

        index: function () {

            if(!localStorage.getItem('currentCupUser') || localStorage.getItem('currentCupUser') === '') {
                this.navigate('login', {trigger: true});
            } else {

                // if (window.history.event.state &&
                //     _.keys(window.history.event.state).length > 0) {
                //     window.history.back();
                // }

                this.navigate('report', {trigger: true});
            }

        },
        login: function () {
            this.pageToLoad = 'login';

            var appView  = new AppView({pageToLoad: this.pageToLoad});
            appView.render();
        },
        logout: function() {
            localStorage.setItem('currentCupUser','');
            this.navigate('login', {trigger: true});
        },
        assortment: function () {
            this.pageToLoad = 'assortment';
            var userPanelView = new UserPanelView();
            userPanelView.render();

            var appView  = new AppView({pageToLoad: this.pageToLoad});
            appView.render();
        },
        report: function () {
            this.pageToLoad = 'report';
            var userPanelView = new UserPanelView();
            userPanelView.render();

            var appView  = new AppView({pageToLoad: this.pageToLoad});
            appView.render();
        },
        statistic: function () {
            this.pageToLoad = 'statistic';

            var appView  = new AppView({pageToLoad: this.pageToLoad});
            appView.render();
        },

        todo: function () {
            this.pageToLoad = 'todo';

            var appView  = new AppView({pageToLoad: this.pageToLoad});
            appView.render();
        },

        viewGoodsById: function (goodsId) {
            console.log('Goods id: ',goodsId )
        },

        defaultRoute: function (goodsId) {
            console.log('defaultRoute')
        }
    });

    return AppRouter
});