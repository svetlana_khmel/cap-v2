define([], function () {
    var Global = function() {
        return {};
    };

    if (!this.global) {
        this.global = new Global();
    }

    return this.global;
});