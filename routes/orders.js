var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var OrderItem = require('../models/orderItem.js');

router.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

function ensureAuthenticated (req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    } else {
        res.json({notLogined: res})
    }
}

/* GET /todos listing. */
router.get('/', function(req, res, next) {
    if(!req.session.user) return;
    OrderItem.find({}, function (err, todos) {
        if (err) return next(err);

        res.json(todos);
    });
});


/* GET /todos/id */
router.get('/:id', function(req, res, next) {
    OrderItem.findById(req.params.id, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});


router.post( '/', function( request, response ) {
   // response.send( 'Library API is running'  );

    var orderItem = new OrderItem({
        timeForOneItem: request.body.timeForOneItem,
        status: request.body.status,
        key: request.body.key,
        models: request.body.models,
        user: request.body.user,
        created_at: request.body.created_at
    });

    orderItem.save( function( err , item) {
        if( !err ) {
            response.json( item );

        } else {
            response.json(err);
        }
    });
});


/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
    OrderItem.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {

    OrderItem.findByIdAndRemove({_id: req.params.id}, function (err, post) {

        if (err) return next(err);
        console.log(req.params, '_____________', post);
        res.json(post);
    });
});

module.exports = router;

