var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var OrderItem = require('../models/orderItem.js');

router.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

function ensureAuthenticated (req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    } else {
        res.json({notLogined: res})
    }
}

/* GET /todos/id */
router.get('/:id', function(req, res, next) {

    OrderItem.findOne({'key': req.params.id}, function(err, post){
        if (err) return next(err);
        //res.json(post);

        res.render('order', {
            title: 'Order status:',
            key: post.key,
            status: post.status,
            models:  post.models
        });
    });

    // OrderItem.findById(req.params.id, function (err, post) {
    //     if (err) return next(err);
    //     //res.json(post);
    //     res.render('order', {
    //         title: 'Order status:',
    //         data: post
    //     });
    // });
});
//db.orderItems.find({"key": {$regex: 'b309'}})

module.exports = router;




/* GET /todos/id */
// router.get('/:id', function(req, res, next) {
//     OrderItem.findById(req.params.id, function (err, post) {
//         if (err) return next(err);
//         res.json(post);
//         //res.render('order', post)
//     });
// });