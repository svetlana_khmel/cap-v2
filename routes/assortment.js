var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var AssortmentItem = require('../models/assortmentItem.js');

router.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

/* GET /todos listing. */
router.get('/', function(req, res, next) {
    AssortmentItem.find({}, function (err, todos) {
        if (err) return next(err);
        res.json(todos);

        console.log(err);
    });
});

// router.post('/', function(req, res, next) {
//     AssortmentItem.create(req.body, function (err, post) {
//         if (err) return next(err);
//         res.json(post);
//     });
// });
// router.post('/api/assortment', function(req, res, next) {
//     res.send( 'Library API is running' );
//     AssortmentItem.create(req.body, function (err, post) {
//         if (err) return next(err);
//         res.json(post);
//     });
//});

router.post( '/', function( request, response ) {

    var assortmentItem = new AssortmentItem({
        name: request.body.name,
        price: request.body.price,
        user: request.body.user

    });

    // assortmentItem.create(request.body, function (err, post) {
    //     if (err) return next(err);
    //     res.json(post);
    //     // res.send(res);
    // });

    assortmentItem.save( function( err , item) {
        if( !err ) {
            response.json( item );
            //return console.log( 'created' );

        } else {
           response.json(err);
        }
    });
});

/* GET /todos/id */
router.get('/:id', function(req, res, next) {
    AssortmentItem.findById(req.params.id, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
    AssortmentItem.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
    AssortmentItem.findByIdAndRemove(req.params.id, function (err, post) {
        console.log('***********  ', req.params.id, '*********   ', err);
        if (err) return next(err);
        res.json(post);
    });
});

module.exports = router;

