var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('../models/user.js');

//var ReportItem = require('../models/ReportItem.js');

//  Authentication

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy(
    function(username, password, done) {
        User.findOne({ username: username }, function (err, user) {

            if (err) { return done(err); }
            if (!user) {
                return done(null, false, { message: 'Incorrect username.' });
            }
            // if (!user.validPassword(password)) {
            //     return done(null, false, { message: 'Incorrect password.' });
            // }
            return done(null, user);
        });
    }
));

// passport.use('registerUser', new LocalStrategy(
//     {passReqToCallback: true},
//     function(username, password, done) {
//
//         User.create(req.body, function (err, post) {
//             if (err) return next(err);
//             res.json(post);
//         });
//
//     }
// ));

router.post('/login', passport.authenticate('local'),
    function (req, res) {
        req.session.user = req.user;
        res.json(req.user);

        //res.status(200).send('Authentication is successfull ', req.user.username);
        //res.redirect('/users/' + req.user.username);
    }
);

passport.serializeUser(function(user, done) {
    console.log('----------------------id   ', user.id);
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {

    if (id.match(/^[0-9a-fA-F]{24}$/)) {
        // Yes, it's a valid ObjectId, proceed with `findById` call.
        console.log('---------------**-------id   ', id);

        User.findById(id, function(err,user){
            err
                ? done(err)
                : done(null,user);
        });
    }

});

/* GET /USERS listing. */
router.get('/', function(req, res, next) {
   // res.send( 'Library API is running'  );
    User.find({}, function (err, todos) {
        if (err) return next(err);
        res.json(todos);

        console.log(err);
    });
});

// router.post('/', function(req, res, next) {
//     //console.log('res1::::::::: ', res);
//
//     User.create(req.body, function (err, post) {
//         if (err) return next(err);
//         res.json(post);
//         // res.send(res);
//     });
// });

router.post( '/', function( request, response ) {

    var user = new User({
        username: request.body.username,
        password: request.body.password
    });

    user.save( function( err , item) {
        if( !err ) {
            response.json( item );
        } else {
            response.json(err);
        }
    });
});

module.exports = router;