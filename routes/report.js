var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var ReportItem = require('../models/ReportItem.js');

router.all('/*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

/* GET /todos listing. */
router.get('/', function(req, res, next) {
    ReportItem.find({}, function (err, todos) {
        if (err) return next(err);
        res.json(todos);

        console.log(err);
    });
});

router.post( '/', function( request, response ) {
    //response.json(response);

    var reportItem = new ReportItem({
        name: request.body.name,
        price: request.body.price,
        type: request.body.type,
        user: request.body.user,
        created_at: request.body.created_at
    });

    reportItem.save( function( err , item) {
        if( !err ) {
            response.json( item );

        } else {
            response.json(err);
        }
    });
});

/* GET /todos/id */
router.get('/:id', function(req, res, next) {
    ReportItem.findById(req.params.id, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* PUT /todos/:id */
router.put('/:id', function(req, res, next) {
    ReportItem.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});

/* DELETE /todos/:id */
router.delete('/:id', function(req, res, next) {
    //res.send(req.param);


    ReportItem.findByIdAndRemove({_id: req.params.id}, function (err, post) {

        if (err) return next(err);
        console.log(req.params, '_____________', post);
        res.json(post);
    });
});

// router.delete('/:id', function(req, res, next) {
//
//     var callback = function (e) {
//         console.log(e);
//         res.send(e);
//     };
//
//     ReportItem.findById(req.query.id, function (err, model) {
//         if (err) {
//             return;
//         }
//
//         if (model === null ) {
//             callback('::::::NO DATA')
//         } else {
//             model.remove(function () {
//                 callback('::::::::DELETED');
//
//                 // if no error, your model is removed
//             });
//         };
//     });
// });

module.exports = router;

